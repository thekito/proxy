<?php

declare(strict_types = 1);
namespace Kito\Proxy;

/**
 * Proxy class for access Memcache or Memcached common functions 
 *
 * @author TheKito
 */
class Memcache
{   
    private $proxy = null;
    
    public function __construct() 
    {
        if(class_exists('\Memcached', false))
            $this->proxy = new \Memcached();
        elseif(class_exists('\Memcache', false))
            $this->proxy = new \Memcache();
        else
            throw new CoreNotFoundException('Memecache');
    }

    public function add(string $key, $var): bool 
    {
        return $this->proxy->add($key, $var);
    }

    public function addServer(string $host, int $port = 11211): bool 
    {
        return $this->proxy->addServer($host, $port);
    }
    
    public function flush(): bool 
    {
        return $this->proxy->flush();
    }

    public function decrement(string $key, int $initial_value = 0): int 
    {
        $key = $key;
        $this->proxy->add($key,$initial_value);
        return $this->proxy->decrement($key);
    }

    public function increment(string $key, int $initial_value = 0): int 
    {
        $key = $key;
        $this->proxy->add($key,$initial_value);
        return $this->proxy->increment($key);        
    }

    public function get(string $key) 
    {
        $_ = $this->proxy->get($key);
        
        if($_===FALSE)
            return null;
        
        return $_;    
    }

    public function set(string $key, $var): bool 
    {
        return $this->proxy->set($key, $var);
    }

    public function delete(string $key): bool 
    {
        return $this->proxy->delete($key);
    }
    
    public function exists(string $key): bool
    {
        return $this->proxy->get($key) !==FALSE ;
    }
    
}
