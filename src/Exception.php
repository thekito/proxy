<?php

namespace Kito\Proxy;

/**
 * Description of Exception
 *
 * @author TheKito < blankitoracing@gmail.com >
 */
class Exception extends \Exception
{
    public function __construct(string $message = "", int $code = 0, \Throwable $previous = null) 
    {
        $classFullName = get_class($this);
        
        if($code===0 || $code===null)
            $code = crc32($classFullName);
        
        parent::__construct($classFullName . ': ' . $message, $code, $previous);
    }
}
